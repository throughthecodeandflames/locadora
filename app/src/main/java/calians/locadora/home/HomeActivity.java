package calians.locadora.home;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import calians.locadora.R;
import calians.locadora.SharedPrefManager;
import calians.locadora.usuario.LoginActivity;
import calians.locadora.usuario.UsuarioController;

public class HomeActivity extends AppCompatActivity {
    private TextView nomeView;
    private ConstraintLayout menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        nomeView = (TextView) findViewById(R.id.nomeView);
        menu = (ConstraintLayout) findViewById(R.id.menu);

        UsuarioController user = SharedPrefManager.getInstance(this).getUser();
        nomeView.setText(String.valueOf(user.getNome()).split(" ")[0]+".");
    }

    public void funcao_menu(View view){
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) menu.getLayoutParams();
        params.setMarginStart(142);
        menu.setLayoutParams(params);
        Log.e("teste",params.toString());
    }
    public void logout(){
        finish();
        SharedPrefManager.getInstance(getApplicationContext()).logout();
    }
}
