package calians.locadora;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

import calians.locadora.home.HomeActivity;
import calians.locadora.usuario.CadastroUsuarioActivity;
import calians.locadora.usuario.LoginActivity;

public class MainActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private ConstraintLayout inicio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = new Intent(this, HomeActivity.class);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        inicio = (ConstraintLayout) findViewById(R.id.inicio);
        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            finish();
                            startActivity(intent);
                        }
                    }, 3000);
        }else{
            progressBar.setVisibility(View.INVISIBLE);
            inicio.setVisibility(View.VISIBLE);
        }
    }

    public void botaoEntrar(View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
    public void botaoCadastrar(View view){
        Intent intent = new Intent(this, CadastroUsuarioActivity.class);
        startActivity(intent);
    }
}
