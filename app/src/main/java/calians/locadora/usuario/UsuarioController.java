package calians.locadora.usuario;

public class UsuarioController {

    private int id_usuario, nivel;
    private String email, nome;

    public UsuarioController(int id_usuario, String email, String nome, int nivel) {
        this.id_usuario = id_usuario;
        this.email = email;
        this.nome = nome;
        this.nivel = nivel;
    }

    public int getIdUsuario() {
        return id_usuario;
    }

    public String getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public int getNivel() {
        return nivel;
    }
}
