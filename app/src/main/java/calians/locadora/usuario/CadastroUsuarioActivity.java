package calians.locadora.usuario;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import calians.locadora.MainActivity;
import calians.locadora.R;
import calians.locadora.SharedPrefManager;
import calians.locadora.VolleySingleton;
import calians.locadora.home.HomeActivity;

import static calians.locadora.URLs.URL_REGISTER_USER;


public class CadastroUsuarioActivity extends AppCompatActivity {

    private EditText editText1, editText2, editText3, editText4;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);


        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        checkBox = (CheckBox) findViewById(R.id.checkBox);


    }

    public void voltar (View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void cadastrar(View view) {
        final String nome = editText1.getText().toString();
        final String email = editText2.getText().toString();
        final String senha = editText3.getText().toString();
        String senha2 = editText4.getText().toString();
        Boolean termos = checkBox.isChecked();
        final Intent intent = new Intent(this, LoginActivity.class);
        final String modelo = Build.MODEL;

        if (TextUtils.isEmpty(nome)) {
            editText1.setError(getResources().getString(R.string.editText1_vazio));
            editText1.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            editText2.setError(getResources().getString(R.string.editText2_vazio));
            editText2.requestFocus();
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editText2.setError(getResources().getString(R.string.editText2_email));
            editText2.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(senha)) {
            editText3.setError(getResources().getString(R.string.editText3_vazio));
            editText3.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(senha2)) {
            editText4.setError(getResources().getString(R.string.editText4_vazio));
            editText4.requestFocus();
            return;
        }

        if (senha.trim().length() < 8) {
            editText3.setError(getResources().getString(R.string.editText3_tamanho));
            editText3.requestFocus();
            return;
        }

        if (senha2.trim().length() < 8) {
            editText4.setError(getResources().getString(R.string.editText4_tamanho));
            editText4.requestFocus();
            return;
        }
        if(!senha.equals(senha2)){
            editText4.setError(getResources().getString(R.string.senha_diferente));
            editText4.getText().clear();
            editText4.requestFocus();
            return;
        }
        if(termos == true){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER_USER,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                if(obj.getBoolean("retorno")){
                                    Toast.makeText(CadastroUsuarioActivity.this, R.string.cadastro_sucesso, Toast.LENGTH_LONG).show();
                                    JSONObject userJson = obj.getJSONObject("usuario");
                                    UsuarioController user = new UsuarioController(
                                            userJson.getInt("id_usuario"),
                                            userJson.getString("email"),
                                            userJson.getString("nome"),
                                            userJson.getInt("nivel")
                                    );
                                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                                    finish();
                                    startActivity(intent);
                                }else if(obj.getInt("num_erro") == 1062){
                                    Toast.makeText(CadastroUsuarioActivity.this, R.string.email_duplicado, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(CadastroUsuarioActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("nome",nome);
                    params.put("email",email);
                    params.put("senha",senha);
                    params.put("modelo",modelo);
                    return params;
                }
            };
            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }else{
            Toast.makeText(CadastroUsuarioActivity.this,R.string.termos_uso,Toast.LENGTH_LONG).show();
        }
    }
}
