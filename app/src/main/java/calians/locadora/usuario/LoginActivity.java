package calians.locadora.usuario;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import calians.locadora.MainActivity;
import calians.locadora.R;
import calians.locadora.SharedPrefManager;
import calians.locadora.VolleySingleton;
import calians.locadora.home.HomeActivity;

import static calians.locadora.URLs.URL_LOGIN;

public class LoginActivity extends AppCompatActivity {

    private EditText editEmail, editSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        }

        editEmail = (EditText) findViewById(R.id.editEmail);
        editSenha = (EditText) findViewById(R.id.editSenha);
    }

    public void voltar (View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }


    public void validar(View view) {
        final String email = editEmail.getText().toString();
        final String senha = editSenha.getText().toString();
        final String modelo = Build.MODEL;

        final Intent intent = new Intent(this, HomeActivity.class);

        if (TextUtils.isEmpty(email)) {
            editEmail.setError(getResources().getString(R.string.editText2_vazio));
            editEmail.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(senha)) {
            editSenha.setError(getResources().getString(R.string.editText3_vazio));
            editSenha.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest (Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("retorno")) {

                                JSONObject userJson = obj.getJSONObject("usuario");
                                UsuarioController user = new UsuarioController(
                                        userJson.getInt("id_usuario"),
                                        userJson.getString("email"),
                                        userJson.getString("nome"),
                                        userJson.getInt("nivel")
                                );
                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_sucesso)+" "+user.getNome(), Toast.LENGTH_LONG).show();
                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                                finish();

                                startActivity(intent);
                            }else {
                                if (obj.getInt("num_erro") == 1) {
                                    Toast.makeText(LoginActivity.this, R.string.login_permissao, Toast.LENGTH_LONG).show();

                                } else if (obj.getInt("num_erro") == 0) {
                                    Toast.makeText(LoginActivity.this, R.string.login_erro, Toast.LENGTH_LONG).show();
                                }
                            }

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("email",email);
                params.put("senha",senha);
                params.put("modelo",modelo);
                return params;
            }

        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
}

