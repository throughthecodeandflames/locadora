package calians.locadora;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;

public class cadastro_titulos extends Activity {

    private EditText tituloPT;
    private EditText anoLancamento;
    private EditText tituloEN;
    private Button salvar;
    private ListView lista;
    private Switch idioma;
    private SQLiteDatabase banco;

    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    String textoTituloExibir = "";

        try{
        tituloEN = (EditText) findViewById(R.id.idTituloEN);
        anoLancamento = (EditText) findViewById(R.id.idLancamento);
        tituloPT = (EditText) findViewById(R.id.idTituloPt);
        lista = (ListView) findViewById(R.id.listTitulos);
        idioma = (Switch) findViewById(R.id.chaveIdioma);
        salvar = (Button) findViewById(R.id.btnSalvar);

        banco = openOrCreateDatabase("videoteca", MODE_PRIVATE, null);

        banco.execSQL("CREATE TABLE IF NOT EXISTS titulos(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "tituloPT VARCHAR, anoLancamento int(4), tituloEN VARCHAR)");


        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulopt = (String) tituloPT.toString();
                String tituloen = (String)  tituloEN.toString();
                int ano = Integer.parseInt(anoLancamento.getText().toString());

                String sqlExecute = "INSERT INTO titulos ( tituloPT , anoLancamento , tituloEN ) " +
                        "values ('"+titulopt+"',"+ano+",'"+tituloen+"')";
                banco.execSQL(sqlExecute);
            }
        });
    }catch (SQLiteException error){
        error.printStackTrace();
    }

}
}


