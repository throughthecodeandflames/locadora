package calians.locadora;

public class URLs {
    private static final String ROOT_URL = "http://locadora-com-br.umbler.net/?banco=";

    public static final String URL_REGISTER_USER = ROOT_URL + "cadastro_usuario";
    public static final String URL_LOGIN= ROOT_URL + "login";
}
